"""SkuVault tap class."""

from typing import List
from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_skuvault.streams import (
    WarehousesStream,
    ProductsStream,
    SalesStream,
    PurchaseOrdersStream,
    WarehousesItemsStream,
    KitsStream,
)

STREAM_TYPES = [
    ProductsStream,
    SalesStream,
    PurchaseOrdersStream,
    WarehousesStream,
    WarehousesItemsStream,
    KitsStream,
]


class TapSkuVault(Tap):
    """SkuVault tap class."""

    name = "tap-skuvault"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "tenant_token",
            th.StringType,
        ),
        th.Property(
            "user_token",
            th.StringType,
        ),
        th.Property(
            "username",
            th.StringType,
        ),
        th.Property(
            "password",
            th.StringType,
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapSkuVault.cli()
