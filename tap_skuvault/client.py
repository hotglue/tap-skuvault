"""REST client handling, including SkuVaultStream base class."""

import requests
import json
from typing import Any, Dict, Optional, Callable
import backoff
from datetime import datetime
from time import sleep
import logging

from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from singer_sdk.streams import RESTStream

logging.getLogger("backoff").setLevel(logging.CRITICAL)


class SkuVaultStream(RESTStream):
    """SkuVault stream class."""

    url_base = "https://app.skuvault.com/api"
    rest_method = "POST"
    tenant_token = ""
    user_token = "user_token"

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        body = json.loads(response.request.body)
        page_number = body.get("PageNumber")

        data = response.json()
        key = next(iter(data))

        if data.get(key):
            next_page_number = page_number + 1
            return next_page_number
        else:
            return

    def get_credentials(self):
        if self.tenant_token and self.user_token:
            return {"tenant_token": self.tenant_token, "user_token": self.user_token}
        elif self.config.get("tenant_token") and self.config.get("user_token"):
            self.tenant_token = self.config.get("tenant_token")
            self.user_token = self.config.get("user_token")
        elif self.config.get("username") and self.config.get("password"):
            headers = {"Accept": "application/json", "Content-Type": "application/json"}
            data = {
                "Email": self.config.get("username"),
                "Password": self.config.get("password"),
            }
            res = requests.post(
                f"{self.url_base}/gettokens", headers=headers, json=data
            ).json()
            if "TenantToken" in res:
                self.tenant_token = res["TenantToken"]
                self.user_token = res["UserToken"]

        return {"tenant_token": self.tenant_token, "user_token": self.user_token}

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        """Prepare the data payload for the REST API request.

        By default, no payload will be sent (return None).
        """
        creds = self.get_credentials()
        payload = {
            "PageNumber": next_page_token or 0,
            "PageSize": 10000,
            "MinimumPageSize": 1000,
            "TenantToken": creds.get("tenant_token"),
            "UserToken": creds.get("user_token"),
        }

        return payload

    def validate_response(self, response: requests.Response) -> None:
        if 400 <= response.status_code < 429:
            msg = (
                f"{response.status_code} Client Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise FatalAPIError(msg)

        elif response.status_code == 429:
            wait_time = response.headers.get("X-RateLimit-Reset")
            if not wait_time:
                sleep(30)
            else:
                wait_time = wait_time.split(":")
                wait_time = (
                    float(wait_time[0]) * 3600
                    + float(wait_time[1]) * 60
                    + float(wait_time[2])
                )
                sleep(wait_time)

        elif 500 <= response.status_code < 600:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise RetriableAPIError(msg)

    def request_decorator(self, func: Callable) -> Callable:
        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.ConnectionError,
            ),
            max_tries=9,
            factor=3,
        )(func)
        return decorator
