"""Stream type classes for tap-skuvault."""

import json
from typing import Any, Optional
from datetime import datetime, timedelta

import requests
import pendulum
from singer_sdk import typing as th

from tap_skuvault.client import SkuVaultStream


class ProductsStream(SkuVaultStream):
    name = "products"
    path = "/products/getProducts"
    primary_keys = ["Id"]
    records_jsonpath = "$.Products[*]"

    schema = th.PropertiesList(
        th.Property("Id", th.StringType),
        th.Property("Code", th.StringType),
        th.Property("Sku", th.StringType),
        th.Property("PrimarySku", th.StringType),
        th.Property("PartNumber", th.StringType),
        th.Property("Description", th.StringType),
        th.Property("ShortDescription", th.StringType),
        th.Property("LongDescription", th.StringType),
        th.Property("Cost", th.NumberType),
        th.Property("RetailPrice", th.NumberType),
        th.Property("SalePrice", th.NumberType),
        th.Property("WeightValue", th.StringType),
        th.Property("WeightUnit", th.StringType),
        th.Property("ReorderPoint", th.NumberType),
        th.Property("Brand", th.StringType),
        th.Property("Supplier", th.StringType),
        th.Property(
            "SupplierInfo",
            th.ArrayType(
                th.ObjectType(
                    th.Property("SupplierName", th.StringType),
                    th.Property("SupplierPartNumber", th.StringType),
                    th.Property("Cost", th.NumberType),
                    th.Property("LeadTime", th.IntegerType),
                    th.Property("IsActive", th.BooleanType),
                    th.Property("IsPrimary", th.BooleanType),
                )
            ),
        ),
        th.Property("Classification", th.StringType),
        th.Property("QuantityOnHand", th.IntegerType),
        th.Property("QuantityOnHold", th.IntegerType),
        th.Property("QuantityPicked", th.IntegerType),
        th.Property("QuantityPending", th.IntegerType),
        th.Property("QuantityAvailable", th.IntegerType),
        th.Property("QuantityIncoming", th.IntegerType),
        th.Property("QuantityInbound", th.IntegerType),
        th.Property("QuantityTransfer", th.IntegerType),
        th.Property("QuantityInStock", th.IntegerType),
        th.Property("QuantityTotalFBA", th.IntegerType),
        th.Property("CreatedDateUtc", th.DateTimeType),
        th.Property("ModifiedDateUtc", th.DateTimeType),
        th.Property("Pictures", th.ArrayType(th.StringType)),
        th.Property(
            "Attributes",
            th.ArrayType(
                th.ObjectType(
                    th.Property("Name", th.StringType),
                    th.Property("Value", th.StringType),
                )
            ),
        ),
        th.Property("VariationParentSku", th.StringType),
        th.Property("IsAlternateSKU", th.BooleanType),
        th.Property("IsAlternateCode", th.BooleanType),
        th.Property("MOQ", th.IntegerType),
        th.Property("MOQInfo", th.StringType),
        th.Property("IncrementalQuantity", th.IntegerType),
        th.Property("DisableQuantitySync", th.BooleanType),
        th.Property("Statuses", th.ArrayType(th.StringType)),
        th.Property("IsSerialized", th.BooleanType),
    ).to_dict()

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        if row.get("MOQ") is not None:
            row["MOQ"] = int(row["MOQ"])

        return row


class SalesStream(SkuVaultStream):
    name = "sales"
    path = "/sales/getSalesByDate"
    primary_keys = ["id"]
    records_jsonpath = "$.[*]"
    # replication_key = "SaleDate"

    schema = th.PropertiesList(
        th.Property("Id", th.StringType),
        th.Property("SellerSaleId", th.BooleanType),
        th.Property("MarketplaceId", th.BooleanType),
        th.Property("ChannelId", th.StringType),
        th.Property("Status", th.StringType),
        th.Property("SaleDate", th.DateTimeType),
        th.Property("Marketplace", th.StringType),
        th.Property(
            "MerchantItems",
            th.ArrayType(
                th.ObjectType(
                    th.Property("Sku", th.StringType),
                    th.Property("Quantity", th.IntegerType),
                    th.Property(
                        "UnitPrice",
                        th.ObjectType(
                            th.Property("a", th.NumberType),
                            th.Property("s", th.StringType),
                        ),
                    ),
                    th.Property("Promotions", th.BooleanType),
                    th.Property("Taxes", th.IntegerType),
                )
            ),
        ),
        th.Property(
            "FulfilledItems",
            th.ArrayType(th.CustomType({"type": ["array", "object", "string"]})),
        ),
        th.Property(
            "SaleKits",
            th.ArrayType(th.CustomType({"type": ["integer", "object", "string"]})),
        ),
        th.Property(
            "FulfilledKits",
            th.ArrayType(th.CustomType({"type": ["array", "object", "string"]})),
        ),
        th.Property(
            "ShippingCost",
            th.ObjectType(
                th.Property("a", th.NumberType),
                th.Property("s", th.StringType),
            ),
        ),
        th.Property(
            "ShippingCharge",
            th.ObjectType(
                th.Property("a", th.NumberType),
                th.Property("s", th.StringType),
            ),
        ),
        th.Property(
            "ShippingInfo",
            th.ObjectType(
                th.Property("City", th.StringType),
                th.Property("Region", th.StringType),
                th.Property("Country", th.StringType),
                th.Property("PostalCode", th.StringType),
                th.Property("Address1", th.StringType),
                th.Property("Address2", th.StringType),
            ),
        ),
        th.Property(
            "ContactInfo",
            th.ObjectType(
                th.Property("FirstName", th.StringType),
                th.Property("LastName", th.StringType),
                th.Property("Company", th.StringType),
                th.Property("Phone", th.StringType),
                th.Property("Email", th.StringType),
            ),
        ),
        th.Property("ShippingCarrier", th.StringType),
        th.Property("ShippingClass", th.StringType),
        th.Property("Notes", th.StringType),
        th.Property("PrintedStatus", th.BooleanType),
        th.Property("LastPrintedDate", th.DateTimeType),
        th.Property("Charges", th.CustomType({"type": ["string", "integer"]})),
        th.Property("Promotions", th.CustomType({"type": ["string", "integer"]})),
    ).to_dict()

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        creds = self.get_credentials()
        payload = {
            "PageSize": 10000,
            "TenantToken": creds.get("tenant_token"),
            "UserToken": creds.get("user_token"),
        }

        if next_page_token is None:
            start_date = self.config.get("start_date", "2010-01-01")
            start_date = pendulum.parse(start_date)
            end_date = start_date + timedelta(7)
            payload["FromDate"] = start_date.strftime("%Y-%m-%d")
            payload["ToDate"] = end_date.strftime("%Y-%m-%d")
            payload["PageNumber"] = 0
        else:
            payload.update(next_page_token)

        return payload

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        body = json.loads(response.request.body)
        start_date = body.get("FromDate")
        end_date = body.get("ToDate")
        page_number = body.get("PageNumber")

        data_len = len(response.json())

        if data_len > 9999:
            next_page_token = {
                "FromDate": start_date,
                "ToDate": end_date,
                "PageNumber": page_number + 1,
            }
            return next_page_token

        start_date = datetime.strptime(start_date, "%Y-%m-%d")
        start_date = start_date + timedelta(7)
        end_date = start_date + timedelta(7)

        if start_date > datetime.now() + timedelta(1):
            return None

        next_page_token = {
            "FromDate": start_date.strftime("%Y-%m-%d"),
            "ToDate": end_date.strftime("%Y-%m-%d"),
            "PageNumber": 0,
        }
        return next_page_token


class PurchaseOrdersStream(SkuVaultStream):
    name = "purchase_orders"
    path = "/purchaseorders/getPOs"
    primary_keys = ["PoId"]
    records_jsonpath = "$.PurchaseOrders[*]"
    partitions = [{"Status": "Everything except Completed"}, {"Status": "Completed"}]

    schema = th.PropertiesList(
        th.Property("PoId", th.StringType),
        th.Property("PoNumber", th.StringType),
        th.Property("Status", th.StringType),
        th.Property("PaymentStatus", th.StringType),
        th.Property("SentStatus", th.StringType),
        th.Property("SupplierName", th.StringType),
        th.Property("CreatedDate", th.DateTimeType),
        th.Property("OrderDate", th.DateTimeType),
        th.Property("OrderCancelDate", th.DateTimeType),
        th.Property("ArrivalDueDate", th.DateTimeType),
        th.Property("RequestedShipDate", th.DateTimeType),
        th.Property("ActualShippedDate", th.DateTimeType),
        th.Property("TrackingInfo", th.StringType),
        th.Property("PublicNotes", th.StringType),
        th.Property("PrivateNotes", th.StringType),
        th.Property("TermsName", th.StringType),
        th.Property("ShipToWarehouse", th.StringType),
        th.Property("ShipToAddress", th.StringType),
        th.Property(
            "ShippingCarrierClass",
            th.ObjectType(
                th.Property("CarrierName", th.StringType),
                th.Property("ClassName", th.StringType),
            ),
        ),
        th.Property(
            "Costs",
            th.ArrayType(
                th.ObjectType(
                    th.Property("Type", th.StringType),
                    th.Property("Amount", th.NumberType),
                    th.Property("Note", th.StringType),
                )
            ),
        ),
        th.Property(
            "LineItems",
            th.ArrayType(
                th.ObjectType(
                    th.Property("ProductId", th.StringType),
                    th.Property("SKU", th.StringType),
                    th.Property("Quantity", th.IntegerType),
                    th.Property("QuantityTo3PL", th.IntegerType),
                    th.Property("ReceivedQuantity", th.IntegerType),
                    th.Property("ReceivedQuantityTo3PL", th.IntegerType),
                    th.Property("Cost", th.NumberType),
                    th.Property("RetailCost", th.NumberType),
                    th.Property("PrivateNotes", th.StringType),
                    th.Property("PublicNotes", th.StringType),
                    th.Property("Variant", th.StringType),
                    th.Property("Identifier", th.StringType),
                )
            ),
        ),
    ).to_dict()

    def prepare_request_payload(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Optional[dict]:
        payload = super().prepare_request_payload(context, next_page_token)
        payload.update(context)
        return payload


class WarehousesStream(SkuVaultStream):
    name = "warehouses"
    path = "/inventory/getWarehouses"
    primary_keys = ["Id"]
    records_jsonpath = "$.Warehouses[*]"

    schema = th.PropertiesList(
        th.Property("Id", th.StringType), th.Property("Code", th.StringType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "WarehouseId": record["Id"],
        }


class WarehousesItemsStream(SkuVaultStream):
    name = "warehouses_items"
    path = "/inventory/getWarehouseItemQuantities"
    primary_keys = ["WarehouseId", "Sku"]
    records_jsonpath = "$.ItemQuantities[*]"
    parent_stream_type = WarehousesStream

    schema = th.PropertiesList(
        th.Property("WarehouseId", th.StringType),
        th.Property("Sku", th.StringType),
        th.Property("Quantity", th.IntegerType),
    ).to_dict()

    def prepare_request_payload(self, context, next_page_token):
        creds = self.get_credentials()
        payload = {
            "WarehouseId": context["WarehouseId"],
            "PageNumber": next_page_token or 0,
            "PageSize": 10000,
            "TenantToken": creds.get("tenant_token"),
            "UserToken": creds.get("user_token"),
        }
        return payload

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        context['WarehouseId'] = str(context['WarehouseId'])
        row.update(context)
        return row


class KitsStream(SkuVaultStream):
    name = "kits"
    path = "/products/getKits"
    primary_keys = ["SKU"]
    records_jsonpath = "$.Kits[*]"
    replication_key = "AvailableQuantityLastModifiedDateTimeUtc"

    schema = th.PropertiesList(
        th.Property("AvailableQuantity", th.NumberType),
        th.Property("AvailableQuantityLastModifiedDateTimeUtc", th.DateTimeType),
        th.Property("Statuses", th.CustomType({"type": ["array", "string"]})),
        th.Property("Code", th.StringType),
        th.Property("Cost", th.NumberType),
        th.Property("Description", th.StringType),
        th.Property("KitLines", th.CustomType({"type": ["array", "string"]})),
        th.Property("LastModifiedDateTimeUtc", th.DateTimeType),
        th.Property("SKU", th.StringType),
    ).to_dict()

    def prepare_request_payload(self, context, next_page_token):
        creds = self.get_credentials()
        payload = {
            "GetAvailableQuantity": True,
            "IncludeKitCost": True,
            "PageNumber": next_page_token or 0,
            "TenantToken": creds.get("tenant_token"),
            "UserToken": creds.get("user_token"),
        }
        # if self.stream_state is not None:
        #     if self.replication_key:
        #         if "replication_key_value" in self.stream_state:
        #             payload.update({"AvailableQuantityModifiedAfterDateTimeUtc":self.stream_state['replication_key_value']})

        return payload
